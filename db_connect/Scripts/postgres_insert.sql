INSERT INTO branches(branch_name, branch_city) 
VALUES ('Филиал в Нижнем Новгороде', 'Нижний Новгород'),
       ('Филиал в Москве','Москва'),
	   ('Филиал в Санкт-Петербурге','Санкт-Петербурге'),
	   ('Филиал в Казани','Казань'),
	   ('Филиал в Хабаровске','Хабаровск');
					 
INSERT INTO clients(client_name, branch_id)
VALUES ('Газпром', (SELECT branch_id FROM branches WHERE branch_name = 'Филиал в Москве')),
       ('Лукойл',(SELECT branch_id FROM branches WHERE branch_name = 'Филиал в Нижнем Новгороде')),
	   ('Татнефть',(SELECT branch_id FROM branches WHERE branch_name = 'Филиал в Казани')),
	   ('РусАгро',(SELECT branch_id FROM branches WHERE branch_name = 'Филиал в Санкт-Петербурге')),
	   ('Совкомфлот',(SELECT branch_id FROM branches WHERE branch_name = 'Филиал в Хабаровске'));
					
INSERT INTO orders(client_id, amount, currency_pair, base_currency, direction)
VALUES ((SELECT client_id FROM clients WHERE client_name = 'Газпром'),1000,'USDRUB','RUB','buy'),
	   ((SELECT client_id FROM clients WHERE client_name = 'Лукойл'),2000,'USDEUR','USD','sell'),
	   ((SELECT client_id FROM clients WHERE client_name = 'Татнефть'),3000,'CNYRUB','RUB','sell'),
	   ((SELECT client_id FROM clients WHERE client_name = 'РусАгро'),4000,'CHFRUB','RUB','buy'),
	   ((SELECT client_id FROM clients WHERE client_name = 'Совкомфлот'),5000,'JPYRUB','JPY','sell');
	   
