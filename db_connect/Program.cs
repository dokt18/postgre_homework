﻿using db_connect.Models;
using Npgsql;
using System;
using System.Linq;

namespace db_connect
{
    public class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "Host=localhost;Username=postgres;Password=12345;Database=Sberbank";
            InsertData(connectionString);
            InsertDataEF();
            ReadData(connectionString);
            Console.WriteLine(new String('*', 100));
            Console.WriteLine("Entity Framework");
            ReadDataEF();
        }
        private static void ReadData(string connectionString)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                Console.WriteLine("Branches table");
                Console.WriteLine("{0,10}|{1,30}|{2,10}", "branch_id", "branch_name", "branch_city");
                using (var command = new NpgsqlCommand("SELECT * FROM branches", connection))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("{0,10}|{1,30}|{2,10}", reader.GetInt32(0), reader.GetString(1), reader.GetString(2));
                    }
                    reader.Close();
                }

                Console.WriteLine("\nClients table");
                Console.WriteLine("\n{0,10}|{1,30}|{2,10}", "client_id", "client_name", "branch");
                using (var command = new NpgsqlCommand("SELECT * FROM clients", connection))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("{0,10}|{1,30}|{2,10}", reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                    }
                    reader.Close();
                }

                Console.WriteLine("\nOrders table");
                Console.WriteLine("\n{0,10}|{1,30}|{2,15}|{3,15}|{4,15}|{5,15}|{6,15}", "order_id", "client", "amount", "currency_pair", "base_currency", "direction", "trade date");
                using (var command = new NpgsqlCommand("SELECT * FROM orders", connection))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("{0,10}|{1,30}|{2,15}|{3,15}|{4,15}|{5,15}|{6,15}", reader.GetInt32(0), reader.GetInt32(1), reader.GetDecimal(2), reader.GetString(3),
                            reader.GetString(4), reader.GetString(5), reader.GetDateTime(6));
                    }
                    reader.Close();
                }
            }
        }

        public static void ReadDataEF()
        {
            using (var db = new SberbankContext())
            {
                Console.WriteLine("{0,10}|{1,30}|{2,10}", "branch_id", "branch_name", "branch_city");
                foreach (var item in db.Branches)
                {
                    Console.WriteLine("{0,10}|{1,30}|{2,10}", item.BranchId, item.BranchName, item.BranchCity);
                }

                Console.WriteLine("\n{0,10}|{1,30}|{2,10}", "client_id", "client_name", "branch");
                foreach (var item in db.Clients)
                {
                    Console.WriteLine("{0,10}|{1,30}|{2,10}", item.ClientId, item.ClientName, item.Branch.BranchName);
                }

                Console.WriteLine("\n{0,10}|{1,30}|{2,15}|{3,15}|{4,15}|{5,15}|{6,15}", "order_id", "client", "amount", "currency_pair", "base_currency", "direction", "trade date");
                foreach (var item in db.Orders)
                {
                    Console.WriteLine("{0,10}|{1,30}|{2,15}|{3,15}|{4,15}|{5,15}|{6,15}", item.OrderId, item.Client.ClientName, item.Amount, item.CurrencyPair,
                        item.BaseCurrency, item.Direction, item.TradeDate);
                }
            }
        }

        private static void InsertData(string connectionString)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                int branch_id = 0;
                using (var command = new NpgsqlCommand("SELECT * FROM branches WHERE branch_name = 'Филиал в Москве'", connection))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        branch_id = reader.GetInt32(0);
                    }
                    reader.Close();
                }

                var sql = @"INSERT INTO clients(client_name, branch_id) VALUES (:client_name, :branch_id)";
                using var cmd = new NpgsqlCommand(sql, connection);
                var parameters = cmd.Parameters;
                parameters.Add(new NpgsqlParameter("client_name", "Пятерочка"));
                parameters.Add(new NpgsqlParameter("branch_id", branch_id));
                cmd.ExecuteNonQuery();
                Console.WriteLine("Client added");
            }
        }
        private static void InsertDataEF()
        {
            using(var db = new SberbankContext())
            {
                var order = new Order();
                order.Client = db.Clients.Where(c => c.ClientName == "Газпром").FirstOrDefault();
                order.Amount = 1500;
                order.CurrencyPair = "USDCNY";
                order.BaseCurrency = "USD";
                order.Direction = "buy";
                db.Orders.Add(order);
                db.SaveChanges();
            }
            Console.WriteLine("Order added");
        }
    }
}
