﻿using System;
using System.Collections.Generic;

#nullable disable

namespace db_connect.Models
{
    public partial class Order
    {
        public int OrderId { get; set; }
        public int ClientId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyPair { get; set; }
        public string BaseCurrency { get; set; }
        public string Direction { get; set; }
        public DateTime? TradeDate { get; set; }

        public virtual Client Client { get; set; }
    }
}
