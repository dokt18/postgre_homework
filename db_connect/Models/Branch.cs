﻿using System;
using System.Collections.Generic;

#nullable disable

namespace db_connect.Models
{
    public partial class Branch
    {
        public Branch()
        {
            Clients = new HashSet<Client>();
        }

        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public string BranchCity { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
    }
}
