﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace db_connect.Models
{
    public partial class SberbankContext : DbContext
    {
        public SberbankContext()
        {
        }

        public SberbankContext(DbContextOptions<SberbankContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Order> Orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Database=Sberbank;Username=postgres;Password=12345");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "English_United States.1251");

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.ToTable("branches");

                entity.HasIndex(e => e.BranchName, "branches_branch_name_key")
                    .IsUnique();

                entity.Property(e => e.BranchId).HasColumnName("branch_id");

                entity.Property(e => e.BranchCity)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("branch_city");

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("branch_name");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("clients");

                entity.HasIndex(e => e.ClientName, "clients_client_name_key")
                    .IsUnique();

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.BranchId).HasColumnName("branch_id");

                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("client_name");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Clients)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("clients_branch_id_fkey");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("orders");

                entity.Property(e => e.OrderId).HasColumnName("order_id");

                entity.Property(e => e.Amount)
                    .HasPrecision(20, 2)
                    .HasColumnName("amount");

                entity.Property(e => e.BaseCurrency)
                    .IsRequired()
                    .HasMaxLength(3)
                    .HasColumnName("base_currency");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.CurrencyPair)
                    .IsRequired()
                    .HasMaxLength(6)
                    .HasColumnName("currency_pair");

                entity.Property(e => e.Direction)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasColumnName("direction");

                entity.Property(e => e.TradeDate)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("trade_date")
                    .HasDefaultValueSql("(CURRENT_TIMESTAMP AT TIME ZONE 'UTC'::text)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_client_id_fkey");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
