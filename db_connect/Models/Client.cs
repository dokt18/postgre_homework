﻿using System;
using System.Collections.Generic;

#nullable disable

namespace db_connect.Models
{
    public partial class Client
    {
        public Client()
        {
            Orders = new HashSet<Order>();
        }

        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
